% Principal Component Analysis (PCA) of Multimodal Myelin-Sensitive MRI.
% In order to extract a single myelin value for further analyses, PCA is used to collapse average values 
% for each modality into a single ‘myelin score’ per subject.
%
% Alberto Lazari, 2021

% Load modalities
MT = load('MT_npc_significant.txt');
R1 = load('R1_npc_significant.txt');
R2 = load('R2_npc_significant.txt');

% PCA on modalities
[coeff,score,~,~,explained] = pca(zscore([MT R1 R2]),'NumComponents',1);