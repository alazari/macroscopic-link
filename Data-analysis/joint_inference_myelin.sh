#!/bin/bash

maptype="MT R1 R2s FA"
analysis=""

studyDir=/vols/Scratch/TBSS/"$cohort"
analysisDir=/vols/Scratch/TBSS/"$cohort"/"$analysis"
mkdir -p $analysisDir

cp $studyDir/"$analysis".mat $analysisDir/"$analysis".mat
cp $studyDir/"$analysis".con $analysisDir/"$analysis".con
cp $studyDir/mean_FA_skeleton_mask.nii $analysisDir/mean_FA_skeleton_mask.nii

for map in $maptype; do

imcp $studyDir/stats/all_"$map"_skeletonised $analysisDir/all_"$map"_skeletonised
gunzip $analysisDir/all_"$map"_skeletonised.nii.gz

done

# set up the stats
inputData1=$analysisDir/all_FA_skeletonised.nii
inputData2=$analysisDir/all_MT_skeletonised.nii
inputData3=$analysisDir/all_R1_skeletonised.nii
inputData4=$analysisDir/all_R2s_skeletonised.nii
mask=$analysisDir/mean_FA_skeleton_mask.nii
stat_mat=$analysisDir/"$analysis".mat
stat_con=$analysisDir/"$analysis".con
outDir=$analysisDir/results
mkdir -p $outDir

cd /vols/Scratch/palm-alpha115/fileio/@file_array/private
./compile.sh
mkoctfile --mex /vols/Scratch/palm-alpha115/fileio/extras/spm_existfile.c

# Run Joint Inference across modalities through Non-Parametric Combinations
/vols/Scratch/palm-alpha115/palm -i $inputData1 -i $inputData2 -i $inputData3 -i $inputData4 -m $mask -d $stat_mat -t $stat_con -o $outDir -n 5000 -npc -C 1.67 -save1-p -corrmod
